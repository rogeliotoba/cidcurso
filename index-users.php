<?php

$user = 'root';
$password = 'root';
$db = 'cid';
$host = 'localhost';
$port = 8889;

$link = mysqli_init();
$success = mysqli_real_connect(
	$link,
	$host,
	$user,
	$password,
	$db,
	$port
);

$users = [];
$usersQuery = mysqli_query($link, "select * from users") or
die("Problemas en el select:" . mysqli_error($link));
while ($reg = mysqli_fetch_array($usersQuery)) {
	$users[] = [
		'id' => $reg['id'],
		'name' => $reg['name'],
		'lat' => $reg['lat'],
		'lon' => $reg['lon'],
	];
}
mysqli_close($link);
echo json_encode($users);